<?php

/**
 * Proper way to enqueue scripts and styles
 */

function iracing_scripts() {
  wp_enqueue_style( 'main-style', get_stylesheet_uri() );

  wp_enqueue_script( 'jquery');
  wp_enqueue_script( 'boostrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );

}

add_action( 'wp_enqueue_scripts', 'iracing_scripts' );



/**
* Proper way to add Logo Upload Capability
*/

function themeslug_theme_customizer( $wp_customize ) {

$wp_customize->add_section( 'themeslug_logo_section' , array(
  'title' => __( 'Logo', 'themeslug' ),
  'priority' => 30,
  'description' => 'Upload a logo to replace the default site name and description in the header',
) );

$wp_customize->add_setting( 'themeslug_logo' );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
  'label' => __( 'Logo', 'themeslug' ),
  'section' => 'themeslug_logo_section',
  'settings' => 'themeslug_logo',
) ) );

}

add_action('customize_register', 'themeslug_theme_customizer');



/**
* Add Menu Support
*/
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );


/**
* Register Footer Area 1
*/
function iracing_widgets_init() {
  // Quick Links - Footer
  register_sidebar( array(
    'name' => 'Quick Links - Footer',
    'id' => 'quick_links_footer',
    'description'   => 'Links to areas of the site for quick access',
    'class'         => '',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ) );

  // About - Footer
  register_sidebar( array(
    'name' => 'About - Footer',
    'id' => 'about_footer',
    'description'   => 'About iRacing links',
    'class'         => '',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ) );

  // e-mail Sign Up - Footer
  register_sidebar( array(
    'name' => 'e-mail Sign Up - Footer',
    'id' => 'e-mail_sign_up_footer',
    'description'   => 'e-mail Sign Up',
    'class'         => '',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ) );

  // advertisment - Sidebar
  register_sidebar( array(
    'name' => 'advertisment - Sidebar',
    'id' => 'advertisement_sidebar',
    'description'   => 'Advertisment in Sidebar. Displayed on cars and truck pages',
    'class'         => '',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ) );


}

add_action( 'widgets_init', 'iracing_widgets_init' );