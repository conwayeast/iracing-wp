<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="A test project for iRacing">
    <meta name="author" content="Ryan Conway">
        <link rel="icon" href="iRacing-favicon.ico">

    <title>iRacing | Welcome</title>

    <!-- Typekit -->
    <script src="https://use.typekit.net/uzo0svq.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <?php wp_head(); ?>
  </head>

  <body>
          <nav class="navbar navbar-inverse">
            <div class="container">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                  <!-- LOGO UPLOAD -->
                   <?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
                      <div class='site-logo'>
                        <a class="navbar-brand" href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img width="120" style="margin-top: 5px;" src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
                      </div>
                    <?php else : ?>
                      <h1 class='site-title'><a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><?php bloginfo( 'name' ); ?></a></h1>
                    <?php endif; ?>
                  <!-- END LOGO UPLOAD-->

              </div><!-- .navbar-header -->

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php wp_nav_menu( array('menu_class' => 'nav navbar-nav')); ?>
              </div><!-- /.navbar-collapse -->

            </div><!-- /.container-fluid -->
          </nav>


