<?php
/* Template Name: Home Page */

get_header(); ?>

<!-- MAIN CONTENT GOES HERE -->

<!-- HERO -->
  <section class="hero">
    <div class="hero-overlay">
      <div class="container hero-content">
       <h1><?php the_field('hero_headline'); ?></h1>
        <p class="lead"><?php the_field('hero_byline'); ?></p>
        <div class="hero-sign-up">
          <a class="btn btn-primary btn-lg" href="<?php the_field('hero_button_link'); ?>"><?php the_field('hero_button_text'); ?></a>
        </div><!-- .hero-sign-up -->
      </div><!-- .hero-overlay -->
    </div><!-- .container hero-content -->
  </section><!-- .hero -->
  <!-- END HERO -->

<!-- QUOTE - Dale Earnhardt Jr -->
  <section class="quote primary edge--top--reverse">

  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="image-wrapper">
          <span class="image-overlay">
            <span class="content">Dale Earnhardt Jr.</span>
          </span>
          <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/dale-earnhardt-jr.jpg" alt="Dale Earnhardt Jr.">
        </div>
      </div><!-- .col-md-2 -->

      <div class="col-md-9">
        <p class="quote-text">iRacing is the most modern racing simulation ever created. For the hardcore sim racer, this is your dream simulation.</p>
        <p class="quote-name">Dale Earnhardt Jr. <span>88</span></p>
        <p class="quote-credentials">NASCAR Sprint Cup Driver / 2-Time NASCAR Nationwide Series Champion (1998, 1999)</p>
      </div><!-- .col-md-10 -->
    </div><!-- .row -->
  </div><!-- .container -->

</section><!-- .quote -->
  <!-- END QUOTE -->


  <!-- CARS -->
  <section class="white edge--top--reverse">
    <div class="container">
      <div class="row">
        <div class="col-md-4 vehicle-card">
          <a href="#" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Toyota-Camry-NASCAR-Sprint-Cup-Car.jpg" alt="">
            </figure>
            <h3>Toyota Camry NASCAR Sprint Cup Car</h3>
          </a>
        </div><!-- .col-md-4 -->

        <div class="col-md-4 vehicle-card">
          <a href="#" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Chevy-Silverado-NASCAR-Camping-World-Truck.jpg" alt="">
            </figure>
            <h3>Chevy Silverado NASCAR Camping World Truck</h3>
          </a>
        </div><!-- .col-md-4 -->

        <div class="col-md-4 vehicle-card">
          <a href="http://localhost/iRacing/iRacing-WP/car/mclaren-honda-mp4-30/" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/McLaren-Honda-MP4-30.jpg" alt="">
            </figure>
            <h3>McLaren Honda MP4-30</h3>
          </a>
        </div><!-- .col-md-4 -->

      </div><!-- .row -->


      <div class="row">
        <div class="col-md-4 vehicle-card">
          <a href="#" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Toyota-Camry-NASCAR-Xfinity-Car.jpg" alt="">
            </figure>
            <h3>Toyota Camry NASCAR Xfinity Car</h3>
          </a>
        </div><!-- .col-md-4 -->

        <div class="col-md-4 vehicle-card">
          <a href="#" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Lotus-49.jpg" alt="">
            </figure>
            <h3>Lotus 49</h3>
          </a>
        </div><!-- .col-md-4 -->

        <div class="col-md-4 vehicle-card">
          <a href="car-detail.php" title="">
            <figure>
              <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Aston-Martin-DBR9-GT1.jpg" alt="">
            </figure>
            <h3>Aston Martin DBR9 GT1</h3>
          </a>
        </div><!-- .col-md-4 -->

      </div><!-- .row -->

      <div class="row">
        <div class="col-sm-12 center">
          <a class="btn btn-default btn-accent btn-lg" href="#" title="">See All Cars &amp; Trucks</a>
        </div>
      </div>

    </div><!-- .container -->
  </section><!-- section -->

<!-- QUOTE -->
  <section class="quote primary edge--top--reverse">

  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="image-wrapper">
          <span class="image-overlay">
            <span class="content">Juan Pablo Montoya</span>
          </span>
          <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/Juan-Pablo-Montoya.jpg" alt="Juan Pablo Montoya">
        </div>
      </div><!-- .col-md-2 -->

      <div class="col-md-9">
        <p class="quote-text">The iRacing simulator is about as real world as you can get when it comes to virtual racing. The graphics and the handling are by far the best I have seen out of any simulation program.</p>
        <p class="quote-name">Juan Pablo Montoya <span>2</span></p>
        <p class="quote-credentials">NASCAR Sprint Cup, Formula One / IndyCar Series &amp; 2013 Rolex 24 Champion </p>
      </div><!-- .col-md-10 -->
    </div><!-- .row -->
  </div><!-- .container -->

</section><!-- .quote -->
  <!-- END QUOTE -->

<!-- MEDIA COVERAGE -->
  <section class="white edge--top--reverse">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>iRacing is Simracing</h3>
          <div class="underline"></div>
        </div>

        <div class="col-md-6">
          <p style="margin-top: 0;">iRacing.com offers the most authentic online racing experience, using the latest technology for its staggering, ever-expanding lineup of famed racecars and courses. All of the painstaking details add up to a robust lineup of cars and tracks that are indistinguishable from the real thing — you’ll be instantly immersed upon entering the cockpit of an iRacing car. Every facet of our simulator is officially licensed and an exact replica of its real-life counterpart.</p>
          <p>The next-generation simracing software is used by professional drivers and casual gamers alike. Although it’s a motorsport simulator at heart, its value as a training tool can’t overshadow the thrilling racing experience that awaits casual gamers looking for a driving experience unlike any other. With its basic hardware requirements, anyone with a compatible PC and steering wheel set, as well as a broadband connection, can join tens-of-thousands of other racers already in our simracing community.</p>
        </div><!-- .col-md-6 -->

        <div class="col-md-6">
          <img src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/iRacing-cockpit.jpg" alt="">
        </div><!-- .col-md-6 -->

      </div><!-- .row -->

      <div class="row">
        <div class="col-md-12">
          <h3>Don't just take our word for it</h3>
          <div class="underline"></div>
        </div>
      </div>

      <div class="row">

        <div class="col-md-4">
          <img width="200" src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/jalopnik-logo.jpg" alt="Jalopnik logo">
          <p style="margin-top: 0;"><span class="bold">“The Most Epic Racing Sim Ever”</span> <br> <a href="http://jalopnik.com/iracings-version-of-the-nurburgring-may-be-the-most-epi-1746919639" target="_blank">jalopnik.com</a> | December 2015 </p>
        </div><!-- .col-md-4 -->

        <div class="col-md-4">
          <img width="200" src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/ESPN-logo.jpg" alt="ESPN">
          <p style="margin-top: 0;"><span class="bold">“A cure for the offseason”</span> <br>
           <a href="http://espn.go.com/racing/story/_/id/12053868/a-little-spent-iracing-helps-pro-drivers-scratch-itch" target="_blank">espn.com</a> | December 2014</p>
        </div><!-- .col-md-4 -->

        <div class="col-md-4">
          <img width="200" src="http://localhost/iRacing/iRacing-WP/wp-content/uploads/2016/01/PC-Gamer-logo.jpg" alt="PC Gamer">
          <p style="margin-top: 0;"><span class="bold">Ranked #39 on PC Gamer’s List of Top 100 Games of All Time.</span> | March 2012</p>
        </div><!-- .col-md-4 -->

      </div>

      </div><!-- .row -->
    </div><!-- .container -->
  </section><!-- section -->

<!-- VIDEO -->
  <section class="primary edge--top--reverse">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h3>See it in action</h3>
          <div class="video-container">
            <iframe width="1150" height="646.88" src="https://www.youtube.com/embed/1gDyEER8pHo" frameborder="0" allowfullscreen></iframe>
          </div><!-- .video-container -->
        </div><!-- .col-sm-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </section><!-- section -->

<!-- FOOTER -->
<?php get_footer(); ?>








