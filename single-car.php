<?php
/* Template Name: Car Template */

get_header(); ?>

  <section>
    <div class="container">
      <div class="row">

        <div class="col-sm-12">
          <h1><?php the_title(); ?></h1>
        </div>

        <!-- MAIN -->
        <div class="col-md-8">
          <img src="<?php the_field('photos_of_car'); ?>" alt="">
          <img src="<?php the_field('photos_of_car_2'); ?>" alt="">
          <img src="<?php the_field('photos_of_car_3'); ?>" alt="">
          <img src="<?php the_field('photos_of_car_4'); ?>" alt="">
          <img src="<?php the_field('photos_of_car_5'); ?>" alt="">
        </div><!-- col-md-8 -->
        <!-- END MAIN -->

        <!-- SIDEBAR -->
        <div class="col-md-4 sidebar primary edge--top--reverse">
          <h3><?php the_field('name_of_car'); ?> Details</h3>
          <?php the_field('description_of_car'); ?>

          <!-- ADVERTISEMENT WIDGET -->
          <div class="sidebar-ad">
            <?php dynamic_sidebar( 'advertisement_sidebar' ); ?>
          </div>

        </div><!-- .col-md-4 -->
        <!-- END SIDEBAR -->

      </div><!-- .row -->
    </div><!-- .container -->
  </section><!-- section -->

  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h3>See it in action</h3>
          <div class="video-container">
            <?php the_field('video'); ?>
          </div><!-- .video-container -->
        </div><!-- .col-sm-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </section><!-- section -->

<?php get_footer(); ?>