<?php /* Template Name: Default Template */

get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 class="entry-header"><?php the_title(); ?></h1>
          <?php the_content(); ?>
        </div><!-- .col-sm-12 -->
      </div><!-- .row -->
    </div><!-- .container -->
  </section>

  <?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>

<?php get_footer(); ?>
