  <footer>
    <div class="container">
      <div class="row">

        <div class="col-md-4">
          <?php dynamic_sidebar( 'quick_links_footer' ); ?>
        </div><!-- .col-md-4 -->

        <div class="col-md-4">
          <?php dynamic_sidebar( 'about_footer' ); ?>
        </div><!-- .col-md-4 -->

        <div class="col-md-4">
          <?php dynamic_sidebar( 'e-mail_sign_up_footer' ); ?>
        </div><!-- .col-md-4 -->

      </div><!-- .row -->
    </div><!-- .container -->
  </footer>

  <?php wp_footer(); ?>

  </body>
</html>