<?php
/* Template Name: Landing Page */

get_header(); ?>

<section class="primary edge--bottom--reverse">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div><!-- .col-sm-12 -->
    </div><!-- .row -->
  </div><!-- .container  -->
</section>


<section>
  <div class="container">
  <div class="row">
      <div class="col-md-4">

        <h2>Contest Rules</h2>
        <p>You'll be automatically entered to win the T550RS Wheel and Pedal Set courtesy of Thrustmaster, if you:</p>
        <ol>
          <li>Place the Thrustmaster Logo on your car (in primary position).</li>
          <span>and</span>
          <li>Race in at least 10 official races between Nov. 25 – Dec 31.</li>
        </ol>
        <p class="bold">That's It!</p>
        <img src="http://www.thrustmaster.com/sites/default/files/imagecache/media_large/product/T500RSproduct-1.jpg" alt="">
        <p><a href="http://www.thrustmaster.com/en_US/products/platforms/pc" target="_blank" title="Thrustmaster products">See the full line of Thrustmaster products</a></p>

      </div><!-- .col-md-4 -->

      <div class="col-md-8">
        <h3>Thrustmaster T150 FFB Racing Wheel - Overview</h3>
        <div class="video-container">
          <iframe width="853" height="480" src="https://www.youtube.com/embed/raqfi8TItvE" frameborder="0" allowfullscreen></iframe>
        </div><!-- .video-container -->

        <div style="padding:1em 0;"></div>

        <h3>Thrustmaster TX Servo Base - Ecosystem overview</h3>
        <div class="video-container">
          <iframe width="853" height="480" src="https://www.youtube.com/embed/fMn8Suu36No" frameborder="0" allowfullscreen></iframe>
        </div><!-- .video-container -->

      </div><!-- .col-md-8 -->



    </div><!-- .row -->
  </div><!-- .container  -->
</section>



<?php get_footer(); ?>